#include <psemek/app/app.hpp>
#include <psemek/app/ui_scene.hpp>
#include <psemek/app/main.hpp>

#include <psemek/gfx/gl.hpp>
#include <psemek/gfx/painter.hpp>
#include <psemek/gfx/program.hpp>
#include <psemek/gfx/array.hpp>
#include <psemek/gfx/buffer.hpp>
#include <psemek/gfx/mesh.hpp>

#include <psemek/random/device.hpp>
#include <psemek/random/generator.hpp>
#include <psemek/random/uniform.hpp>

#include <psemek/geom/interval.hpp>
#include <psemek/geom/box.hpp>
#include <psemek/geom/camera.hpp>
#include <psemek/geom/rotation.hpp>
#include <psemek/geom/intersection.hpp>
#include <psemek/geom/distance.hpp>
#include <psemek/geom/swizzle.hpp>

#include <psemek/util/array.hpp>
#include <psemek/util/clock.hpp>
#include <psemek/util/hash.hpp>
#include <psemek/util/to_string.hpp>

#include <psemek/ui/default_element_factory.hpp>

#include <psemek/audio/engine.hpp>

#include <unordered_map>
#include <deque>
#include <iomanip>

#include <tanks/resources/track_mp3.hpp>
#include <tanks/resources/click_wav.hpp>
#include <tanks/resources/explosion_wav.hpp>
#include <tanks/resources/shoot_wav.hpp>
#include <tanks/resources/upgrade_wav.hpp>

using namespace psemek;

geom::interval<int> const ROOM_SIZE_RANGE = {11, 25};

geom::point<int, 2> const STARTING_ROOM_ID{0, 0};

float const ACCELERATION = 10.f;
float const GROUND_FRICTION = 3.f;
float const BULLET_VELOCITY = 20.f;
float const EXPLOSION_DURATION = 0.25f;
float const EXPLOSION_RADIUS = 2.f;

std::pair<float, geom::vector<float, 2>> separation(
	geom::box<float, 2> const & b1, geom::point<float, 2> const & p1, float a1, float s1,
	geom::box<float, 2> const & b2, geom::point<float, 2> const & p2, float a2, float s2)
{
	geom::vector<float, 2> directions[4];
	directions[0] = geom::direction(a1);
	directions[1] = geom::ort(directions[0]);
	directions[2] = geom::direction(a2);
	directions[3] = geom::ort(directions[2]);

	float max_distance = -std::numeric_limits<float>::infinity();
	geom::vector<float, 2> max_direction;

	geom::plane_rotation<float, 2> const r1(0, 1, a1);
	geom::plane_rotation<float, 2> const r2(0, 1, a2);

	geom::point<float, 2> const o{0.f, 0.f};

	for (auto const & d : directions)
	{
		geom::interval<float> i1, i2;

		i1 |= geom::dot(p1 + r1(b1.corner(0.f, 0.f) - o) * s1 - o, d);
		i1 |= geom::dot(p1 + r1(b1.corner(1.f, 0.f) - o) * s1 - o, d);
		i1 |= geom::dot(p1 + r1(b1.corner(0.f, 1.f) - o) * s1 - o, d);
		i1 |= geom::dot(p1 + r1(b1.corner(1.f, 1.f) - o) * s1 - o, d);

		i2 |= geom::dot(p2 + r2(b2.corner(0.f, 0.f) - o) * s2 - o, d);
		i2 |= geom::dot(p2 + r2(b2.corner(1.f, 0.f) - o) * s2 - o, d);
		i2 |= geom::dot(p2 + r2(b2.corner(0.f, 1.f) - o) * s2 - o, d);
		i2 |= geom::dot(p2 + r2(b2.corner(1.f, 1.f) - o) * s2 - o, d);

		auto d12 = i2.min - i1.max;
		auto d21 = i1.min - i2.max;

		float dist;
		geom::vector<float, 2> n;

		if (d12 > d21)
		{
			dist = d12;
			n = d;
		}
		else
		{
			dist = d21;
			n = -d;
		}

		if (dist > max_distance)
		{
			max_distance = dist;
			max_direction = n;
		}
	}

	return {max_distance, max_direction};
}

enum class tile
{
	ground,
	water,
	wall,
	barricade,

	count
};

bool passable(tile t)
{
	switch (t)
	{
	case tile::ground: return true;
	case tile::water: return false;
	case tile::wall: return false;
	case tile::barricade: return false;
	default: return false;
	}
}

bool solid(tile t)
{
	switch (t)
	{
	case tile::ground: return false;
	case tile::water: return false;
	case tile::wall: return true;
	case tile::barricade: return true;
	default: return false;
	}
}

bool destructible(tile t)
{
	switch (t)
	{
	case tile::ground: return false;
	case tile::water: return false;
	case tile::wall: return false;
	case tile::barricade: return true;
	default: return false;
	}
}

struct tank
{
	geom::point<float, 2> position;
	gfx::color_3f color;
	float direction = geom::pi / 2.f;
	float turret_direction = 0.f;
	float velocity = 0.f;
	float bullet_reload = 1.f;
	float size = 1.f;
	float health = 1.f;

	float rotation_speed = 3.f;
	float max_velocity = 5.f;
	float reload_time = 0.5f;
	float armor = 1.f;

	float turret_rotation_speed = 5.f;
};

struct cannon
{
	geom::point<float, 2> position;
	float turret_direction = 0.f;
	float bullet_reload = 0.f;
	float health = 1.f;

	static constexpr float max_bullet_reload = 1.f;
	static constexpr float max_rotate_speed = 1.f;
};

struct bullet
{
	geom::point<float, 2> position;
	geom::vector<float, 2> velocity;
	bool player;
};

struct explosion
{
	geom::point<float, 2> position;
	float duration = 0.f;
};

struct medicine
{
	geom::point<float, 2> position;
};

struct upgrade
{
	geom::point<float, 2> position;
	enum type_t
	{
		velocity,
		reload,
		armor,

		count,
	} type;
};

struct autosave
{
	geom::point<float, 2> position;
};

struct crystal
{
	geom::point<float, 2> position;
};

struct room
{
	util::array<tile, 2> tiles;
	util::array<float, 2> tiles_hp;
	bool exits[4]{};

	std::vector<cannon> cannons;
	std::vector<medicine> medicines;
	std::vector<upgrade> upgrades;
	std::vector<autosave> autosaves;

	std::optional<::crystal> crystal;

	room() = default;
	room(room &&) = default;

	room(room const & r)
	{
		*this = r;
	}

	room & operator = (room &&) = default;

	room & operator = (room const & r)
	{
		if (this == &r) return *this;

		tiles = r.tiles.copy();
		tiles_hp = r.tiles_hp.copy();
		for (int i = 0; i < 4; ++i) exits[i] = r.exits[i];
		cannons = r.cannons;
		medicines = r.medicines;
		upgrades = r.upgrades;
		autosaves = r.autosaves;

		return *this;
	}

	geom::box<float, 2> bbox() const
	{
		return {{{0.f, tiles.width() * 1.f}, {0.f, tiles.height() * 1.f}}};
	}
};

struct save_state
{
	std::unordered_map<geom::point<int, 2>, room> rooms;
	geom::point<int, 2> current_room;
	tank player;
	int free_exits;
};

void add_border_walls(util::array<tile, 2> & tiles, bool const (&exits)[4])
{
	for (int x = 0; x < tiles.width(); ++x)
	{
		tiles(x, 0) = tile::wall;
		tiles(x, tiles.height() - 1) = tile::wall;
	}

	for (int y = 0; y < tiles.height(); ++y)
	{
		tiles(0, y) = tile::wall;
		tiles(tiles.width() - 1, y) = tile::wall;
	}

	int xc = (tiles.width() - 1) / 2;
	int yc = (tiles.height() - 1) / 2;

	if (exits[0])
	{
		tiles(tiles.width() - 1, yc - 1) = tile::ground;
		tiles(tiles.width() - 1, yc    ) = tile::ground;
		tiles(tiles.width() - 1, yc + 1) = tile::ground;
	}

	if (exits[2])
	{
		tiles(0, yc - 1) = tile::ground;
		tiles(0, yc    ) = tile::ground;
		tiles(0, yc + 1) = tile::ground;
	}

	if (exits[1])
	{
		tiles(xc - 1, tiles.height() - 1) = tile::ground;
		tiles(xc    , tiles.height() - 1) = tile::ground;
		tiles(xc + 1, tiles.height() - 1) = tile::ground;
	}

	if (exits[3])
	{
		tiles(xc - 1, 0) = tile::ground;
		tiles(xc    , 0) = tile::ground;
		tiles(xc + 1, 0) = tile::ground;
	}
}

room make_starting_room()
{
	room r;
	r.tiles.resize({17, 17}, tile::ground);
	r.exits[1] = true;
	add_border_walls(r.tiles, r.exits);

	r.tiles_hp.resize(r.tiles.dims(), 1.f);

	int s = 4;

	for (int x = s; x + s < r.tiles.width(); ++x)
	{
		r.tiles(x, s) = tile::barricade;
		r.tiles(x, r.tiles.height() - s - 1) = tile::barricade;
	}

	for (int y = s; y + s < r.tiles.height(); ++y)
	{
		r.tiles(s, y) = tile::barricade;
		r.tiles(r.tiles.width() - s - 1, y) = tile::barricade;
	}

	for (int x = 2; x + 2 < r.tiles.width(); ++x)
	{
		if (x != 7 && x != 8 && x != 9)
			r.tiles(x, r.tiles.height() - 3) = tile::water;
	}

	return r;
}

static char const lit_vs[] =
R"(#version 330

uniform mat4 u_transform;
uniform vec2 u_translation;
uniform vec2 u_rotation;
uniform float u_scale;

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec4 in_color;

out vec4 color;
out vec3 normal;

vec2 rot(vec2 a, vec2 b)
{
	return vec2(a.x * b.x - a.y * b.y, a.y * b.x + a.x * b.y);
}

void main()
{
	gl_Position = u_transform * vec4(rot(u_rotation, in_position) * u_scale + u_translation, 0.0, 1.0);
	normal = vec3(rot(u_rotation, in_normal.xy), in_normal.z);
	color = in_color;
}
)";

static char const lit_fs[] =
R"(#version 330

uniform vec4 u_color;
uniform vec3 u_light_dir;

in vec4 color;
in vec3 normal;

out vec4 out_color;

void main()
{
	float l = dot(normalize(normal), u_light_dir) * 0.5 + 0.5;

	out_color = vec4(color.rgb * l * u_color.rgb, color.a * u_color.a);
}
)";

struct lit_vertex
{
	geom::point<float, 2> position;
	geom::vector<float, 3> normal;
	gfx::color_rgba color;
};

gfx::mesh create_lit_mesh()
{
	gfx::mesh m;
	m.setup<geom::point<float, 2>, geom::vector<float, 3>, gfx::normalized<gfx::color_rgba>>();
	return m;
}

struct effect_text
{
	std::string text;
	geom::point<float, 2> position;
	gfx::color_rgba color;
	float duration = 0.f;
};

struct tanks_scene
	: app::ui_scene
{
	tanks_scene(ui::controller & controller, std::function<void()> on_quit);

	void on_resize(int width, int height) override;
	void on_key_down(SDL_Keycode key) override;

	void update() override;
	void present() override;

	void create_tile_meshes();
	void create_tank_mesh();
	void create_bullet_mesh();
	void create_explosion_mesh();
	void create_cannon_mesh();
	void create_item_meshes();

	void switch_room(int direction);
	void update_minimap();

	void save();
	void load();
	void new_game();

	void show_death_dialog();
	void show_victory_dialog();
	void show_esc_dialog();
	void remove_dialog();
	bool has_dialog();

	std::unordered_map<geom::point<int, 2>, room> rooms;
	geom::point<int, 2> current_room_id = STARTING_ROOM_ID;

	tank player_tank;

	save_state autosave;

	std::vector<bullet> bullets;

	std::vector<explosion> explosions;

	util::clock<std::chrono::duration<float>, std::chrono::high_resolution_clock> clock;
	float time = 0.f;

	ui::default_element_factory element_factory;
	std::shared_ptr<ui::screen> ui_root;

	std::vector<effect_text> effect_texts;

	std::uint64_t map_seed{0};
	int free_exits = 0;

	random::generator rng;

	geom::box<float, 2> view_bbox;

	geom::box<float, 2> tile_bbox;
	geom::box<float, 2> tank_bbox;

	gfx::painter painter;

	gfx::program lit_program{lit_vs, lit_fs};

	std::unordered_map<tile, gfx::mesh> tile_mesh;
	gfx::mesh tile_fade_mesh;
	gfx::mesh tile_black_mesh;
	gfx::mesh tank_mesh;
	gfx::mesh turret_mesh;
	gfx::mesh bullet_mesh;
	gfx::mesh explosion_mesh;
	gfx::mesh cannon_mesh;
	gfx::mesh medicine_mesh;
	gfx::mesh upgrade_mesh;
	gfx::mesh autosave_mesh;
	gfx::mesh crystal_mesh;

	gfx::texture_2d minimap_texture;

	std::function<void()> on_quit;

	audio::engine audio_engine;
	std::shared_ptr<audio::track> click_track;
	std::shared_ptr<audio::track> explosion_track;
	std::shared_ptr<audio::track> shoot_track;
	std::shared_ptr<audio::track> upgrade_track;
};

tanks_scene::tanks_scene(ui::controller & controller, std::function<void()> on_quit)
	: app::ui_scene(controller)
	, on_quit(on_quit)
{
	create_tile_meshes();
	create_tank_mesh();
	create_bullet_mesh();
	create_explosion_mesh();
	create_cannon_mesh();
	create_item_meshes();

	minimap_texture.nearest_filter();
	minimap_texture.clamp();

	auto style = std::make_shared<ui::style>(ui::default_style());
	style->font = ui::make_default_9x12_font();
	style->text_scale = 2;
	ui_root = element_factory.make_screen();
	ui_root->set_style(style);
	set_ui(ui_root);

	audio_engine.play(audio_engine.load(tanks::resources::track_mp3), true, true);

	click_track = audio_engine.load(tanks::resources::click_wav);
	explosion_track = audio_engine.load(tanks::resources::explosion_wav);
	shoot_track = audio_engine.load(tanks::resources::shoot_wav);
	upgrade_track = audio_engine.load(tanks::resources::upgrade_wav);

	new_game();
}

geom::interval<float> make_interval(float center, float size)
{
	return {center - size, center + size};
}

void tanks_scene::on_resize(int width, int height)
{
	ui_scene::on_resize(width, height);

	auto const & current_room = rooms.at(current_room_id);

	if (width > height)
	{
		view_bbox[1] = make_interval(current_room.tiles.height() / 2.f, ROOM_SIZE_RANGE.max / 2.f);
		view_bbox[0] = make_interval(current_room.tiles.width() / 2.f, (width * 1.f) / height * ROOM_SIZE_RANGE.max / 2.f);
	}
	else
	{
		view_bbox[1] = make_interval(current_room.tiles.height() / 2.f, (height * 1.f) / width * ROOM_SIZE_RANGE.max / 2.f);
		view_bbox[0] = make_interval(current_room.tiles.width() / 2.f, ROOM_SIZE_RANGE.max / 2.f);
	}
}

void tanks_scene::on_key_down(SDL_Keycode key)
{
	ui_scene::on_key_down(key);

	if (key == SDLK_ESCAPE)
	{
		if (!has_dialog())
			show_esc_dialog();
	}
}

void tanks_scene::update()
{
	float const dt = clock.restart().count();

	time += dt;

	if (has_dialog())
	{
		ui_scene::update();
		return;
	}

	auto & current_room = rooms.at(current_room_id);

	geom::box<float, 2> room_bbox = current_room.bbox();

	std::optional<int> accelerating;

	if (is_key_down(SDLK_w) || is_key_down(SDLK_UP))
	{
		player_tank.velocity += ACCELERATION * dt;
		accelerating = 1;
	}

	if (is_key_down(SDLK_s) || is_key_down(SDLK_DOWN))
	{
		player_tank.velocity -= ACCELERATION * dt;
		accelerating = -1;
	}

	float const rotation_speed = player_tank.rotation_speed * std::exp(- std::abs(player_tank.velocity) / player_tank.max_velocity);
	float const rotation_dir = accelerating ? (*accelerating * 1.f) : 1.f;

	if (is_key_down(SDLK_a) || is_key_down(SDLK_LEFT))
	{
		player_tank.direction += dt * rotation_speed * rotation_dir;
	}

	if (is_key_down(SDLK_d) || is_key_down(SDLK_RIGHT))
	{
		player_tank.direction -= dt * rotation_speed * rotation_dir;
	}

	auto player_dir = geom::direction(player_tank.direction);

	if (!accelerating)
	{
		player_tank.velocity -= player_tank.velocity * GROUND_FRICTION * dt;
	}

	player_tank.velocity = geom::clamp(player_tank.velocity, {-player_tank.max_velocity, player_tank.max_velocity});
	player_tank.position += player_dir * player_tank.velocity * dt;

	if (auto ms = mouse())
	{
		geom::point<float, 2> m = view_bbox.corner((*ms)[0] * 1.f / width(), 1.f - (*ms)[1] * 1.f / height());

		auto target_direction = geom::angle(m - player_tank.position);

		auto d = geom::angle_difference(player_tank.turret_direction, target_direction);

		if (std::abs(d) > player_tank.turret_rotation_speed * dt)
			d *= player_tank.turret_rotation_speed * dt / std::abs(d);

		player_tank.turret_direction += d;
	}

	if (current_room.exits[0] && player_tank.position[0] > current_room.tiles.width() - 0.25f)
	{
		switch_room(0);
		return;
	}

	if (current_room.exits[2] && player_tank.position[0] < 0.25f)
	{
		switch_room(2);
		return;
	}

	if (current_room.exits[1] && player_tank.position[1] > current_room.tiles.height() - 0.25f)
	{
		switch_room(1);
		return;
	}

	if (current_room.exits[3] && player_tank.position[1] < 0.25f)
	{
		switch_room(3);
		return;
	}

	for (int y = 0; y < current_room.tiles.height(); ++y)
	{
		for (int x = 0; x < current_room.tiles.width(); ++x)
		{
			if (!passable(current_room.tiles(x, y)))
			{
				auto sep = separation(tile_bbox, {x + 0.5f, y + 0.5f}, 0.f, 1.f, tank_bbox, player_tank.position, player_tank.direction, player_tank.size);
				if (sep.first < 0.f)
				{
					player_tank.position -= sep.second * sep.first;
					auto vel = player_tank.velocity * player_dir;
					vel -= sep.second * geom::dot(sep.second, vel);
					player_tank.velocity = geom::dot(player_dir, vel);

					if (destructible(current_room.tiles(x, y)))
					{
						if (random::uniform<float>(rng, {0.f, 1.f}) < 0.1f)
						{
							current_room.tiles_hp(x, y) -= 0.5f;
							if (current_room.tiles_hp(x, y) <= 0.f)
							{
								current_room.tiles_hp(x, y) = 1.f;
								current_room.tiles(x, y) = tile::ground;
							}
						}
					}
				}
			}
		}
	}

	for (auto & c : current_room.cannons)
	{
		auto sep = separation({{{-0.4f, 0.4f}, {-0.4f, 0.4f}}}, c.position, 0.f, 1.f, tank_bbox, player_tank.position, player_tank.direction, player_tank.size);
		if (sep.first < 0.f)
		{
			player_tank.position -= sep.second * sep.first;
			auto vel = player_tank.velocity * player_dir;
			vel -= sep.second * geom::dot(sep.second, vel);
			player_tank.velocity = geom::dot(player_dir, vel);
		}
	}

	if (player_tank.bullet_reload > 0.f)
		player_tank.bullet_reload -= dt;

	if (player_tank.bullet_reload <= 0.f && is_left_button_down())
	{
		player_tank.bullet_reload = player_tank.reload_time;
		auto dir = geom::direction(player_tank.turret_direction);
		bullets.push_back({player_tank.position + dir * tank_bbox[0].max, dir * BULLET_VELOCITY, true});
		audio_engine.play(shoot_track);
	}

	for (auto & c : current_room.cannons)
	{
		if (c.bullet_reload > 0.f)
			c.bullet_reload -= dt;

		geom::simplex seg{c.position, player_tank.position};

		bool visible = true;

		for (int y = 0; y < current_room.tiles.height(); ++y)
		{
			for (int x = 0; x < current_room.tiles.width(); ++x)
			{
				if (solid(current_room.tiles(x, y)))
				{
					auto ion = geom::intersection(seg, geom::box<float, 2>{{{x + 0.f, x + 1.f}, {y + 0.f, y + 1.f}}});
					if (!ion.empty())
					{
						visible = false;
						break;
					}
				}
			}

			if (!visible)
				break;
		}

		if (visible)
		{
			auto target_dir = geom::angle(player_tank.position - c.position);

			auto d = geom::angle_difference(c.turret_direction, target_dir);

			bool can_shoot = std::abs(d) < geom::pi / 6.f;

			if (std::abs(d) > c.max_rotate_speed * dt)
				d *= c.max_rotate_speed * dt / std::abs(d);

			c.turret_direction += d;

			if (c.bullet_reload <= 0.f && can_shoot)
			{
				c.bullet_reload = c.max_bullet_reload;
				auto dir = geom::direction(c.turret_direction);
				bullets.push_back({c.position + dir, dir * BULLET_VELOCITY, false});
				audio_engine.play(shoot_track);
			}
		}
	}

	std::vector<bullet> alive_bullets;

	for (auto & b : bullets)
	{
		auto new_pos = b.position + b.velocity * dt;
		geom::simplex s{b.position, new_pos};

		std::optional<float> collision;

		for (int y = 0; y < current_room.tiles.height(); ++y)
		{
			for (int x = 0; x < current_room.tiles.width(); ++x)
			{
				if (solid(current_room.tiles(x, y)))
				{
					auto bbox = tile_bbox;
					bbox[0] += x + 0.5f;
					bbox[1] += y + 0.5f;

					auto ion = geom::intersection(s, bbox);
					if (!ion.empty())
					{
						collision = ion.min;
						break;
					}
				}
			}

			if (collision)
				break;
		}

		if (!b.player && !collision)
		{
			geom::plane_rotation<float, 2> rot(0, 1, -player_tank.direction);

			static geom::point<float, 2> const o{0.f, 0.f};

			auto seg = s;
			seg[0] = o + rot(seg[0] - player_tank.position);
			seg[1] = o + rot(seg[1] - player_tank.position);

			auto ion = geom::intersection(seg, tank_bbox);
			if (!ion.empty())
			{
				collision = ion.min;
			}
		}

		if (b.player && !collision)
		{
			for (auto const & c : current_room.cannons)
			{
				if (geom::distance(c.position, s) < 0.5f)
				{
					collision = 0.5f;
					break;
				}
			}
		}

		if (!collision)
		{
			if (geom::contains(room_bbox, new_pos))
			{
				b.position = new_pos;
				alive_bullets.push_back(b);
			}
		}
		else
		{
			auto pos = b.position + b.velocity * dt * (*collision);
			explosions.push_back({pos});
			audio_engine.play(explosion_track);

			for (int y = 0; y < current_room.tiles.height(); ++y)
			{
				for (int x = 0; x < current_room.tiles.width(); ++x)
				{
					if (destructible(current_room.tiles(x, y)))
					{
						geom::point p{x + 0.5f, y + 0.5f};
						auto d = geom::distance(p, pos);
						if (d < EXPLOSION_RADIUS)
						{
							float m = 1.f - d / EXPLOSION_RADIUS;
							current_room.tiles_hp(x, y) -= m * 1.5f;
							if (current_room.tiles_hp(x, y) <= 0.f)
							{
								current_room.tiles_hp(x, y) = 1.f;
								current_room.tiles(x, y) = tile::ground;
							}
						}
					}
				}
			}

			{
				auto d = geom::distance(pos, player_tank.position);
				if (d < EXPLOSION_RADIUS)
				{
					float m = 1.f - d / EXPLOSION_RADIUS;
					player_tank.health -= m / player_tank.armor;
				}
			}

			for (auto & c : current_room.cannons)
			{
				auto d = geom::distance(pos, c.position);
				if (d < EXPLOSION_RADIUS)
				{
					float m = 1.f - d / EXPLOSION_RADIUS;
					c.health = std::max(0.f, c.health - m);
				}
			}
		}
	}

	bullets = std::move(alive_bullets);

	std::vector<explosion> alive_explosions;

	for (auto & e : explosions)
	{
		e.duration += dt;

		if (e.duration < EXPLOSION_DURATION)
			alive_explosions.push_back(e);
	}

	explosions = std::move(alive_explosions);

	std::vector<cannon> alive_cannons;
	for (auto & c : current_room.cannons)
	{
		if (c.health > 0.f)
			alive_cannons.push_back(c);
	}
	current_room.cannons = std::move(alive_cannons);

	std::vector<medicine> alive_medicines;
	for (auto const & m : current_room.medicines)
	{
		auto sep = separation({{{-0.4f, 0.4f}, {-0.4f, 0.4f}}}, m.position, 0.f, 1.f, tank_bbox, player_tank.position, player_tank.direction, player_tank.size);
		if (sep.first <= 0.f && player_tank.health < 1.f)
		{
			player_tank.health = 1.f;
			effect_texts.push_back({"Health +100%", player_tank.position + geom::vector{0.f, -0.5f}, gfx::red.as_color_rgba()});
			audio_engine.play(upgrade_track);
		}
		else
		{
			alive_medicines.push_back(m);
		}
	}
	current_room.medicines = std::move(alive_medicines);

	bool victory = false;

	if (auto const & c = current_room.crystal)
	{
		auto sep = separation({{{-0.4f, 0.4f}, {-0.4f, 0.4f}}}, c->position, 0.f, 1.f, tank_bbox, player_tank.position, player_tank.direction, player_tank.size);
		if (sep.first <= 0.f)
		{
			victory = true;
			audio_engine.play(upgrade_track);
		}
	}

	std::vector<upgrade> alive_upgrades;
	for (auto const & u : current_room.upgrades)
	{
		auto sep = separation({{{-0.4f, 0.4f}, {-0.4f, 0.4f}}}, u.position, 0.f, 1.f, tank_bbox, player_tank.position, player_tank.direction, player_tank.size);
		if (sep.first <= 0.f)
		{
			switch (u.type)
			{
			case upgrade::velocity:
				player_tank.max_velocity += 1.f;
				player_tank.rotation_speed += 0.5f;
				effect_texts.push_back({"Velocity +1", player_tank.position + geom::vector{0.f, -0.5f}, gfx::dark(gfx::green).as_color_rgba()});
				break;
			case upgrade::reload:
				player_tank.reload_time = 1.f / (1.f / player_tank.reload_time + 1.f);
				effect_texts.push_back({"Shooting speed +1", player_tank.position + geom::vector{0.f, -0.5f}, gfx::dark(gfx::green).as_color_rgba()});
				break;
			case upgrade::armor:
				player_tank.armor += 1.f;
				effect_texts.push_back({"Armor +1", player_tank.position + geom::vector{0.f, -0.5f}, gfx::dark(gfx::green).as_color_rgba()});
				break;
			default:
				break;
			}
			audio_engine.play(upgrade_track);
		}
		else
		{
			alive_upgrades.push_back(u);
		}
	}
	current_room.upgrades = std::move(alive_upgrades);

	std::vector<::autosave> alive_autosaves;
	bool need_save = false;
	for (auto const & a : current_room.autosaves)
	{
		auto sep = separation({{{-0.4f, 0.4f}, {-0.4f, 0.4f}}}, a.position, 0.f, 1.f, tank_bbox, player_tank.position, player_tank.direction, player_tank.size);
		if (sep.first <= 0.f)
		{
			need_save = true;
			effect_texts.push_back({"Game saved", player_tank.position + geom::vector{0.f, -0.5f}, gfx::dark(gfx::blue).as_color_rgba()});
			audio_engine.play(upgrade_track);
		}
		else
		{
			alive_autosaves.push_back(a);
		}
	}
	current_room.autosaves = std::move(alive_autosaves);

	if (victory)
	{
		show_victory_dialog();
	}

	if (player_tank.health <= 0.f)
	{
		show_death_dialog();
	}
	else if (need_save)
	{
		save();
		update_minimap();
	}

	std::vector<effect_text> alive_effect_texts;
	for (auto & t : effect_texts)
	{
		t.duration += dt;
		t.position[1] += dt;
		if (t.duration < 1.f)
			alive_effect_texts.push_back(std::move(t));
	}
	effect_texts = std::move(alive_effect_texts);
}

void tanks_scene::present()
{
	gl::ClearColor(0.f, 0.f, 0.f, 0.f);
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	gl::Viewport(0, 0, width(), height());

	auto & current_room = rooms.at(current_room_id);

	auto const transform = geom::orthographic_camera{view_bbox}.transform();

	auto const light_dir = geom::normalized(geom::vector{1.f, 2.f, 1.f});
	auto const water_light_dir = geom::normalized(geom::vector{std::cos(time * 2.f) * 2.f, std::sin(time * 2.f) * 2.f, 1.f});

	gl::Enable(gl::BLEND);
	gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);

	gl::Disable(gl::DEPTH_TEST);
	gl::Disable(gl::CULL_FACE);

	lit_program.bind();
	lit_program["u_transform"] = transform;

	lit_program["u_scale"] = 1.f;
	lit_program["u_rotation"] = geom::vector{1.f, 0.f};
	lit_program["u_light_dir"] = light_dir;

	for (int y = 0; y < current_room.tiles.height(); ++y)
	{
		for (int x = 0; x < current_room.tiles.width(); ++x)
		{
			lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
			if (current_room.tiles(x, y) == tile::water)
				lit_program["u_light_dir"] = water_light_dir;
			else
				lit_program["u_light_dir"] = light_dir;

			if (destructible(current_room.tiles(x, y)))
			{
				lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f};
				tile_mesh[tile::ground].draw();
			}

			lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, current_room.tiles_hp(x, y) * 0.5f + 0.5f};
			tile_mesh[current_room.tiles(x, y)].draw();
		}
	}
	lit_program["u_light_dir"] = light_dir;

	for (auto const & b : bullets)
	{
		lit_program["u_rotation"] = geom::normalized(b.velocity);
		lit_program["u_scale"] = 0.3f;
		lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f};
		lit_program["u_translation"] = b.position;
		bullet_mesh.draw();
	}

	auto const draw_tank = [&](tank const & t)
	{
		lit_program["u_rotation"] = geom::vector{std::cos(t.direction), std::sin(t.direction)};
		lit_program["u_scale"] = t.size;
		lit_program["u_color"] = gfx::color_4f{0.1f, 0.5f, 0.1f, 1.f};
		lit_program["u_translation"] = t.position;
		tank_mesh.draw();

		lit_program["u_rotation"] = geom::vector{std::cos(t.turret_direction), std::sin(t.turret_direction)};
		turret_mesh.draw();
	};

	draw_tank(player_tank);

	for (auto const & c : current_room.cannons)
	{
		lit_program["u_rotation"] = geom::vector{1.f, 0.f};
		lit_program["u_scale"] = 1.f;
		lit_program["u_color"] = gfx::color_4f{0.5f, 0.5f, 0.5f, 1.f};
		lit_program["u_translation"] = c.position;
		cannon_mesh.draw();

		lit_program["u_rotation"] = geom::vector{std::cos(c.turret_direction), std::sin(c.turret_direction)};
		lit_program["u_scale"] = 1.5f;
		turret_mesh.draw();
	}

	for (auto const & c : current_room.medicines)
	{
		lit_program["u_rotation"] = geom::vector{1.f, 0.f};
		lit_program["u_scale"] = geom::lerp({0.8f, 1.f}, std::sin(time * 2.f) * 0.5f + 0.5f);
		lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f};
		lit_program["u_translation"] = c.position;
		medicine_mesh.draw();
	}

	for (auto const & c : current_room.upgrades)
	{
		lit_program["u_rotation"] = geom::vector{1.f, 0.f};
		lit_program["u_scale"] = geom::lerp({0.8f, 1.f}, std::sin(time * 2.f) * 0.5f + 0.5f);
		lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f};
		lit_program["u_translation"] = c.position;
		upgrade_mesh.draw();
	}

	for (auto const & a : current_room.autosaves)
	{
		lit_program["u_rotation"] = geom::vector{1.f, 0.f};
		lit_program["u_scale"] = geom::lerp({0.8f, 1.f}, std::sin(time * 2.f) * 0.5f + 0.5f);
		lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f};
		lit_program["u_translation"] = a.position;
		autosave_mesh.draw();
	}

	if (auto const & c = current_room.crystal)
	{
		lit_program["u_rotation"] = geom::vector{std::cos(time * 3.f), std::sin(time * 3.f)};
		lit_program["u_scale"] = geom::lerp({0.8f, 1.f}, std::sin(time * 2.f) * 0.5f + 0.5f);
		lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f};
		lit_program["u_translation"] = (*c).position;
		crystal_mesh.draw();
	}

	lit_program["u_scale"] = 1.f;
	lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f};

	for (int y = 0; y < current_room.tiles.height(); ++y)
	{
		for (int x = 0; x < current_room.tiles.width(); ++x)
		{
			if (!solid(current_room.tiles(x, y)))
			{
				if (x == 0)
				{
					lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
					lit_program["u_rotation"] = geom::vector{-1.f, 0.f};
					tile_fade_mesh.draw();
				}

				if (x == current_room.tiles.width() - 1)
				{
					lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
					lit_program["u_rotation"] = geom::vector{1.f, 0.f};
					tile_fade_mesh.draw();
				}

				if (y == 0)
				{
					lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
					lit_program["u_rotation"] = geom::vector{0.f, -1.f};
					tile_fade_mesh.draw();
				}

				if (y == current_room.tiles.height() - 1)
				{
					lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
					lit_program["u_rotation"] = geom::vector{0.f, 1.f};
					tile_fade_mesh.draw();
				}
			}
		}
	}

	for (int x = -1; x < static_cast<int>(current_room.tiles.width() + 1); ++x)
	{
		int y = -1;
		lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
		tile_black_mesh.draw();

		y = current_room.tiles.height();
		lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
		tile_black_mesh.draw();
	}

	for (int y = -1; y < static_cast<int>(current_room.tiles.height() + 1); ++y)
	{
		int x = -1;
		lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
		tile_black_mesh.draw();

		x = current_room.tiles.width();
		lit_program["u_translation"] = geom::vector{x + 0.5f, y + 0.5f};
		tile_black_mesh.draw();
	}

	for (auto const & e : explosions)
	{
		lit_program["u_rotation"] = geom::vector{1.f, 0.f};
		lit_program["u_scale"] = std::sqrt(e.duration / EXPLOSION_DURATION) * EXPLOSION_RADIUS;
		lit_program["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 1.f - e.duration / EXPLOSION_DURATION};
		lit_program["u_translation"] = e.position;
		explosion_mesh.draw();
	}

	auto draw_health = [&](geom::point<float, 2> const & pos, float health)
	{
		auto v = geom::unlerp(view_bbox, pos);
		v[1] = 1.f - v[1];

		auto p = geom::lerp(geom::box<float, 2>{{{0.f, width()}, {0.f, height()}}}, v);

		int w = 20;
		int h = 3;

		painter.rect({{{p[0] - w - 1, p[0] + w + 1}, {p[1] - h - 1, p[1] + h + 1}}}, {0, 0, 0, 127});

		painter.rect({{{p[0] - w, p[0] - w + 2.f * w * health}, {p[1] - h, p[1] + h}}}, {255, 0, 0, 191});
	};

	for (auto const & c : current_room.cannons)
	{
		if (c.health >= 1.f) continue;

		draw_health(c.position + geom::vector{0.f, -0.4f}, c.health);
	}

	if (player_tank.health < 1.f)
	{
		draw_health(player_tank.position + geom::vector{0.f, -0.4f}, player_tank.health);
	}

	for (auto const & t : effect_texts)
	{
		int a = std::max<int>(0, 255 * (1.f - t.duration));

		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::center;
		opts.y = gfx::painter::y_align::center;

		auto v = geom::unlerp(view_bbox, t.position);
		v[1] = 1.f - v[1];

		auto p = geom::lerp(geom::box<float, 2>{{{0.f, width()}, {0.f, height()}}}, v);

		opts.c = gfx::black;
		opts.c[3] = a;
		painter.text(p + geom::vector{1.f, 1.f}, t.text, opts);

		opts.c = t.color;
		opts.c[3] = a;
		painter.text(p, t.text, opts);
	}

	{
		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::left;
		opts.y = gfx::painter::y_align::top;
		opts.c = gfx::white;

		auto str = util::to_string("Health: ", std::round(player_tank.health * 100.f));
		painter.text({20.f, 20.f}, str, opts);

		str = util::to_string("Speed: ", player_tank.max_velocity);
		painter.text({20.f, 44.f}, str, opts);

		str = util::to_string("Shooting speed: ", 1.f / player_tank.reload_time);
		painter.text({20.f, 68.f}, str, opts);

		str = util::to_string("Armor: ", player_tank.armor);
		painter.text({20.f, 92.f}, str, opts);
	}

	if (current_room_id == STARTING_ROOM_ID)
	{
		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::center;
		opts.y = gfx::painter::y_align::top;

		int x = width() / 2;
		int y = 12;

		std::string str = "Move with WASD/arrows";
		opts.c = gfx::black;
		painter.text({x + 1, y + 1}, str, opts);
		opts.c = gfx::white;
		painter.text({x, y}, str, opts);

		y += 24;
		str = "Aim & shoot with mouse";
		opts.c = gfx::black;
		painter.text({x + 1, y + 1}, str, opts);
		opts.c = gfx::white;
		painter.text({x, y}, str, opts);

		y += 24;
		str = "Find the red crystal!";
		opts.c = gfx::black;
		painter.text({x + 1, y + 1}, str, opts);
		opts.c = gfx::white;
		painter.text({x, y}, str, opts);

		y += 24;
		str = "Move up to the next room";
		opts.c = gfx::black;
		painter.text({x + 1, y + 1}, str, opts);
		opts.c = gfx::white;
		painter.text({x, y}, str, opts);
	}

	{
		float const s = 2.f;
		geom::box<float, 2> b;
		b[0] = {width() - s * minimap_texture.width() - 50.f, width() - 50.f};
		b[1] = {height() / 2.f + s * minimap_texture.height() / 2.f, height() / 2.f - s * minimap_texture.height() / 2.f};
		painter.texture(minimap_texture, b);

		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::center;
		opts.y = gfx::painter::y_align::bottom;

		opts.c = gfx::white;
		painter.text({b[0].center(), b[1].max - 12.f}, "Minimap", opts);
	}

	{
		gfx::painter::text_options opts;
		opts.scale = 1.f;
		opts.x = gfx::painter::x_align::center;
		opts.y = gfx::painter::y_align::bottom;
		opts.c = {255, 255, 0, 255};

		int x = width() / 2;
		int y = height() - 36;

		painter.text({x, y}, "Made by lisyarus (lisyarus.itch.io)", opts);

		y += 12;
		painter.text({x, y}, "Music by Vitling (vitling.xyz)", opts);

		y += 12;
		painter.text({x, y}, "LDJAM-48", opts);
	}


	painter.render(geom::window_camera{width(), height()}.transform());

	ui_scene::present();
}

void tanks_scene::create_tile_meshes()
{
	tile_bbox = {{{-0.5f, 0.5f}, {-0.5f, 0.5f}}};

	for (int i = 0; i < static_cast<int>(tile::count); ++i)
		tile_mesh[static_cast<tile>(i)] = create_lit_mesh();

	float const w = 0.5f;

	{
		std::vector<lit_vertex> vertices;
		geom::vector n{0.f, 0.f, 1.f};
		gfx::color_rgba c{181, 116, 42, 255};

		random::generator rng{4, 0};

		int const N = 16;
		util::array<geom::vector<float, 3>, 2> ns({N, N});
		for (auto & n : ns)
		{
			float s = 0.2f;
			n[0] = random::uniform<float>(rng, {-s, s});
			n[1] = random::uniform<float>(rng, {-s, s});
			n[2] = 1.f;
			n = geom::normalized(n);
		}

		float d = 1.f / N;

		for (int y = 0; y < N; ++y)
		{
			for (int x = 0; x < N; ++x)
			{
				lit_vertex v0, v1, v2, v3;

				v0.position = {-0.5f + (x + 0) * d, -0.5f + (y + 0) * d};
				v1.position = {-0.5f + (x + 1) * d, -0.5f + (y + 0) * d};
				v2.position = {-0.5f + (x + 0) * d, -0.5f + (y + 1) * d};
				v3.position = {-0.5f + (x + 1) * d, -0.5f + (y + 1) * d};

				v0.color = c;
				v1.color = c;
				v2.color = c;
				v3.color = c;

				v0.normal = ns((x + 0) % N, (y + 0) % N);
				v1.normal = ns((x + 1) % N, (y + 0) % N);
				v2.normal = ns((x + 0) % N, (y + 1) % N);
				v3.normal = ns((x + 1) % N, (y + 1) % N);

				vertices.push_back(v0);
				vertices.push_back(v1);
				vertices.push_back(v2);
				vertices.push_back(v2);
				vertices.push_back(v1);
				vertices.push_back(v3);
			}
		}

		tile_mesh[tile::ground].load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		std::vector<lit_vertex> vertices;
		geom::vector n{0.f, 0.f, 1.f};
		gfx::color_rgba c{19, 100, 180, 255};

		random::generator rng{5, 0};

		int const N = 8;
		util::array<geom::vector<float, 3>, 2> ns({N, N});
		for (auto & n : ns)
		{
			float s = 0.5f;
			n[0] = random::uniform<float>(rng, {-s, s});
			n[1] = random::uniform<float>(rng, {-s, s});
			n[2] = 1.f;
			n = geom::normalized(n);
		}

		float d = 1.f / N;

		for (int y = 0; y < N; ++y)
		{
			for (int x = 0; x < N; ++x)
			{
				lit_vertex v0, v1, v2, v3;

				v0.position = {-0.5f + (x + 0) * d, -0.5f + (y + 0) * d};
				v1.position = {-0.5f + (x + 1) * d, -0.5f + (y + 0) * d};
				v2.position = {-0.5f + (x + 0) * d, -0.5f + (y + 1) * d};
				v3.position = {-0.5f + (x + 1) * d, -0.5f + (y + 1) * d};

				v0.color = c;
				v1.color = c;
				v2.color = c;
				v3.color = c;

				v0.normal = ns((x + 0) % N, (y + 0) % N);
				v1.normal = ns((x + 1) % N, (y + 0) % N);
				v2.normal = ns((x + 0) % N, (y + 1) % N);
				v3.normal = ns((x + 1) % N, (y + 1) % N);

				vertices.push_back(v0);
				vertices.push_back(v1);
				vertices.push_back(v2);
				vertices.push_back(v2);
				vertices.push_back(v1);
				vertices.push_back(v3);
			}
		}

		tile_mesh[tile::water].load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		std::vector<lit_vertex> vertices;
		gfx::color_rgba c{127, 127, 127, 255};
		geom::vector<float, 3> n;

		float const a = 0.3f;

		// center
		n = {0.f, 0.f, 1.f};
		vertices.push_back({{-a, -a}, n, c});
		vertices.push_back({{ a, -a}, n, c});
		vertices.push_back({{-a,  a}, n, c});
		vertices.push_back({{-a,  a}, n, c});
		vertices.push_back({{ a, -a}, n, c});
		vertices.push_back({{ a,  a}, n, c});

		// left
		n = geom::normalized(geom::vector{-1.f, 0.f, 1.f});
		vertices.push_back({{-w, -w}, n, c});
		vertices.push_back({{-a, -a}, n, c});
		vertices.push_back({{-w,  w}, n, c});
		vertices.push_back({{-w,  w}, n, c});
		vertices.push_back({{-a, -a}, n, c});
		vertices.push_back({{-a,  a}, n, c});

		// right
		n = geom::normalized(geom::vector{ 1.f, 0.f, 1.f});
		vertices.push_back({{ w, -w}, n, c});
		vertices.push_back({{ w,  w}, n, c});
		vertices.push_back({{ a, -a}, n, c});
		vertices.push_back({{ a, -a}, n, c});
		vertices.push_back({{ w,  w}, n, c});
		vertices.push_back({{ a,  a}, n, c});

		// bottom
		n = geom::normalized(geom::vector{0.f, -1.f, 1.f});
		vertices.push_back({{-w, -w}, n, c});
		vertices.push_back({{ w, -w}, n, c});
		vertices.push_back({{-a, -a}, n, c});
		vertices.push_back({{-a, -a}, n, c});
		vertices.push_back({{ w, -w}, n, c});
		vertices.push_back({{ a, -a}, n, c});

		// top
		n = geom::normalized(geom::vector{0.f,  1.f, 1.f});
		vertices.push_back({{-w,  w}, n, c});
		vertices.push_back({{-a,  a}, n, c});
		vertices.push_back({{ w,  w}, n, c});
		vertices.push_back({{-a,  a}, n, c});
		vertices.push_back({{ a,  a}, n, c});
		vertices.push_back({{ w,  w}, n, c});

		tile_mesh[tile::wall].load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		gfx::color_rgba c{148, 54, 13, 255};

		float const a = 0.4f;
		float const b = 0.3f;
		float const d = -0.1f;

		std::vector<geom::point<float, 3>> points
		{
			{-w, -w, 0.f},
			{ w, -w, 0.f},
			{-w,  w, 0.f},
			{ w,  w, 0.f},

			{-a, -a, 0.f},
			{ a, -a, 0.f},
			{-a,  a, 0.f},
			{ a,  a, 0.f},

			{-b, -b, d},
			{ b, -b, d},
			{-b,  b, d},
			{ b,  b, d},
		};

		std::vector<std::size_t> triangles
		{
			0, 1, 4,
			1, 4, 5,
			2, 3, 6,
			6, 3, 7,
			0, 2, 4,
			4, 2, 6,
			1, 3, 5,
			3, 5, 7,

			4, 5, 8,
			5, 8, 9,
			6, 7, 10,
			10, 7, 11,
			4, 6, 8,
			8, 6, 10,
			5, 7, 9,
			7, 9, 11,

			8, 9, 10,
			9, 10, 11,
		};

		std::vector<lit_vertex> vertices;

		for (std::size_t i = 0; i < triangles.size(); i += 3)
		{
			auto p0 = points[triangles[i + 0]];
			auto p1 = points[triangles[i + 1]];
			auto p2 = points[triangles[i + 2]];

			auto n = geom::normal(p0, p1, p2);
			if (n[2] < 0.f) n = -n;

			vertices.push_back({geom::swizzle<0, 1>(p0), n, c});
			vertices.push_back({geom::swizzle<0, 1>(p1), n, c});
			vertices.push_back({geom::swizzle<0, 1>(p2), n, c});
		}

		tile_mesh[tile::barricade].load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		tile_fade_mesh = create_lit_mesh();

		geom::vector n{0.f, 0.f, 1.f};

		std::vector<lit_vertex> vertices;
		vertices.push_back({{ -w, -w}, n, {0, 0, 0,   0}});
		vertices.push_back({{  w, -w}, n, {0, 0, 0, 255}});
		vertices.push_back({{ -w,  w}, n, {0, 0, 0,   0}});
		vertices.push_back({{ -w,  w}, n, {0, 0, 0,   0}});
		vertices.push_back({{  w, -w}, n, {0, 0, 0, 255}});
		vertices.push_back({{  w,  w}, n, {0, 0, 0, 255}});
		tile_fade_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		tile_black_mesh = create_lit_mesh();

		geom::vector n{0.f, 0.f, 1.f};
		gfx::color_rgba c{0, 0, 0, 255};

		std::vector<lit_vertex> vertices;
		vertices.push_back({{ -w, -w}, n, c});
		vertices.push_back({{  w, -w}, n, c});
		vertices.push_back({{ -w,  w}, n, c});
		vertices.push_back({{ -w,  w}, n, c});
		vertices.push_back({{  w, -w}, n, c});
		vertices.push_back({{  w,  w}, n, c});
		tile_black_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}
}

void tanks_scene::create_tank_mesh()
{
	tank_bbox = {{{-0.45f, 0.35f}, {-0.3f, 0.3f}}};

	tank_mesh = create_lit_mesh();

	std::vector<lit_vertex> vertices;
	geom::vector<float, 3> n;
	gfx::color_rgba c = {255, 255, 255, 255};

	// center
	n = {0.f, 0.f, 1.f};
	vertices.push_back({{-0.2f, -0.2f}, n, c});
	vertices.push_back({{ 0.2f, -0.2f}, n, c});
	vertices.push_back({{-0.2f,  0.2f}, n, c});
	vertices.push_back({{-0.2f,  0.2f}, n, c});
	vertices.push_back({{ 0.2f, -0.2f}, n, c});
	vertices.push_back({{ 0.2f,  0.2f}, n, c});

	// back plate
	n = geom::normalized(geom::vector{0.f, -1.f, 1.f});
	vertices.push_back({{-0.2f, -0.4f}, n, c});
	vertices.push_back({{ 0.2f, -0.4f}, n, c});
	vertices.push_back({{-0.2f, -0.2f}, n, c});
	vertices.push_back({{-0.2f, -0.2f}, n, c});
	vertices.push_back({{ 0.2f, -0.4f}, n, c});
	vertices.push_back({{ 0.2f, -0.2f}, n, c});

	// front plate
	n = geom::normalized(geom::vector{0.f, 1.f, 2.f});
	vertices.push_back({{-0.2f,  0.2f}, n, c});
	vertices.push_back({{ 0.2f,  0.2f}, n, c});
	vertices.push_back({{-0.2f,  0.3f}, n, c});
	vertices.push_back({{-0.2f,  0.3f}, n, c});
	vertices.push_back({{ 0.2f,  0.2f}, n, c});
	vertices.push_back({{ 0.2f,  0.3f}, n, c});

	// tracks
	for (int i = 0; i < 4; ++i)
	{
		float y = -0.45f + i * 0.2f;

		for (int s : {-1, 1})
		{
			vertices.push_back({{-0.3f * s, y + 0.0f}, geom::normalized(geom::vector{-s, -1.f, 1.f}), c});
			vertices.push_back({{-0.2f * s, y + 0.0f}, geom::normalized(geom::vector{-s, -1.f, 1.f}), c});
			vertices.push_back({{-0.3f * s, y + 0.1f}, geom::normalized(geom::vector{-s,  0.f, 1.f}), c});
			vertices.push_back({{-0.3f * s, y + 0.1f}, geom::normalized(geom::vector{-s,  0.f, 1.f}), c});
			vertices.push_back({{-0.2f * s, y + 0.0f}, geom::normalized(geom::vector{-s, -1.f, 1.f}), c});
			vertices.push_back({{-0.2f * s, y + 0.1f}, geom::normalized(geom::vector{-s,  0.f, 1.f}), c});

			vertices.push_back({{-0.3f * s, y + 0.1f}, geom::normalized(geom::vector{-s,  0.f, 1.f}), c});
			vertices.push_back({{-0.2f * s, y + 0.1f}, geom::normalized(geom::vector{-s,  0.f, 1.f}), c});
			vertices.push_back({{-0.3f * s, y + 0.2f}, geom::normalized(geom::vector{-s,  1.f, 1.f}), c});
			vertices.push_back({{-0.3f * s, y + 0.2f}, geom::normalized(geom::vector{-s,  1.f, 1.f}), c});
			vertices.push_back({{-0.2f * s, y + 0.1f}, geom::normalized(geom::vector{-s,  0.f, 1.f}), c});
			vertices.push_back({{-0.2f * s, y + 0.2f}, geom::normalized(geom::vector{-s,  1.f, 1.f}), c});
		}
	}

	for (auto & v : vertices)
	{
		v.position = {v.position[1], -v.position[0]};
		v.normal = {v.normal[1], -v.normal[0], v.normal[2]};
	}

	tank_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);

	vertices.clear();

	turret_mesh = create_lit_mesh();

	float const r = 0.15f;

	float const l = 0.5f;
	float const w = 0.04f;

	vertices.push_back({{ -w, 0.f}, {-1.f, 0.f, 0.f}, c});
	vertices.push_back({{0.f, 0.f}, { 0.f, 0.f, 1.f}, c});
	vertices.push_back({{ -w,   l}, {-1.f, 0.f, 0.f}, c});
	vertices.push_back({{ -w,   l}, {-1.f, 0.f, 0.f}, c});
	vertices.push_back({{0.f, 0.f}, { 0.f, 0.f, 1.f}, c});
	vertices.push_back({{0.f,   l}, { 0.f, 0.f, 1.f}, c});
	vertices.push_back({{  w, 0.f}, { 1.f, 0.f, 0.f}, c});
	vertices.push_back({{0.f, 0.f}, { 0.f, 0.f, 1.f}, c});
	vertices.push_back({{  w,   l}, { 1.f, 0.f, 0.f}, c});
	vertices.push_back({{  w,   l}, { 1.f, 0.f, 0.f}, c});
	vertices.push_back({{0.f, 0.f}, { 0.f, 0.f, 1.f}, c});
	vertices.push_back({{0.f,   l}, { 0.f, 0.f, 1.f}, c});

	int const N = 6;

	for (int i = 0; i < N; ++i)
	{
		float const a = geom::pi * i * 2.f / N;
		float const b = geom::pi * (i + 1) * 2.f / N;

		auto const da = geom::direction(a);
		auto const db = geom::direction(b);

		geom::point const o{0.f, 0.f};

		vertices.push_back({{0.f, 0.f}, {0.f, 0.f, 1.f}, c});
		vertices.push_back({o + da * r, {da[0], da[1], 0.f}, c});
		vertices.push_back({o + db * r, {db[0], db[1], 0.f}, c});
	}

	for (auto & v : vertices)
	{
		v.position = {v.position[1], -v.position[0]};
		v.normal = {v.normal[1], -v.normal[0], v.normal[2]};
	}

	turret_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
}

void tanks_scene::create_bullet_mesh()
{
	bullet_mesh = create_lit_mesh();

	std::vector<lit_vertex> vertices;

	gfx::color_rgba c{224, 114, 29, 255};

	for (int s : {-1, 1})
	{
		vertices.push_back({{0.20f * s, -0.2f}, {1.f * s, 0.f, 0.f}, c});
		vertices.push_back({{0.00f * s, -0.2f}, {0.f * s, 0.f, 1.f}, c});
		vertices.push_back({{0.20f * s,  0.0f}, {1.f * s, 0.f, 0.f}, c});
		vertices.push_back({{0.20f * s,  0.0f}, {1.f * s, 0.f, 0.f}, c});
		vertices.push_back({{0.00f * s, -0.2f}, {0.f * s, 0.f, 1.f}, c});
		vertices.push_back({{0.00f * s,  0.0f}, {0.f * s, 0.f, 1.f}, c});

		vertices.push_back({{0.20f * s,  0.0f}, {1.f * s, 0.f, 0.f}, c});
		vertices.push_back({{0.00f * s,  0.0f}, {0.f * s, 0.f, 1.f}, c});
		vertices.push_back({{0.15f * s,  0.2f}, {1.f * s, 0.f, 0.f}, c});
		vertices.push_back({{0.15f * s,  0.2f}, {1.f * s, 0.f, 0.f}, c});
		vertices.push_back({{0.00f * s,  0.0f}, {0.f * s, 0.f, 1.f}, c});
		vertices.push_back({{0.00f * s,  0.2f}, {0.f * s, 0.f, 1.f}, c});

		vertices.push_back({{0.15f * s,  0.2f}, {1.f * s, 0.f, 0.f}, c});
		vertices.push_back({{0.00f * s,  0.2f}, {0.f * s, 0.f, 1.f}, c});
		vertices.push_back({{0.00f * s,  0.3f}, {0.f * s, 0.f, 1.f}, c});

		vertices.push_back({{0.20f * s, -0.2f}, {0.f, 0.f, 1.f}, {255, 255, 255, 0}});
		vertices.push_back({{0.00f * s, -0.2f}, {0.f, 0.f, 1.f}, {255, 255, 255, 127}});
		vertices.push_back({{0.00f * s, -2.5f}, {0.f, 0.f, 1.f}, {255, 255, 255, 0}});
	}

	for (auto & v : vertices)
	{
		v.position = {v.position[1], -v.position[0]};
		v.normal = {v.normal[1], -v.normal[0], v.normal[2]};
	}

	bullet_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
}

void tanks_scene::create_explosion_mesh()
{
	explosion_mesh = create_lit_mesh();

	std::vector<lit_vertex> vertices;
	int const N = 24;

	float const r = 1.f;

	for (int i = 0; i < N; ++i)
	{
		float const a = geom::pi * i * 2.f / N;
		float const b = geom::pi * (i + 1) * 2.f / N;

		auto const da = geom::direction(a);
		auto const db = geom::direction(b);

		geom::point const o{0.f, 0.f};

		vertices.push_back({{0.f, 0.f}, {0.f, 0.f, 1.f}, {255, 255, 0, 255}});
		vertices.push_back({o + da * r, {da[0], da[1], 0.f}, {255, 0, 0, 127}});
		vertices.push_back({o + db * r, {db[0], db[1], 0.f}, {255, 0, 0, 127}});
	}

	explosion_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
}

void tanks_scene::create_cannon_mesh()
{
	cannon_mesh = create_lit_mesh();

	std::vector<lit_vertex> vertices;

	gfx::color_rgba c{255, 255, 255, 255};

	int const N = 6;
	float const r1 = 0.3f;
	float const r2 = 0.45f;
	for (int i = 0; i < N; ++i)
	{
		auto const start = vertices.size();

		auto da = geom::direction<float>((-0.5f) * 2.f * geom::pi / N);
		auto db = geom::direction<float>(( 0.5f) * 2.f * geom::pi / N);

		static geom::point<float, 2> const o{0.f, 0.f};

		vertices.push_back({o, {0.f, 0.f, 1.f}, c});
		vertices.push_back({o + da * r1, {0.f, 0.f, 1.f}, c});
		vertices.push_back({o + db * r1, {0.f, 0.f, 1.f}, c});

		auto n = geom::normalized(geom::vector{1.f, 0.f, 1.f});

		vertices.push_back({o + da * r1, n, c});
		vertices.push_back({o + db * r1, n, c});
		vertices.push_back({o + da * r2, n, c});
		vertices.push_back({o + da * r2, n, c});
		vertices.push_back({o + db * r1, n, c});
		vertices.push_back({o + db * r2, n, c});

		geom::plane_rotation<float, 2> rot(0, 1, i * 2.f * geom::pi / N);

		for (std::size_t i = start; i < vertices.size(); ++i)
		{
			auto & v = vertices[i];

			auto n = rot(geom::swizzle<0, 1>(v.normal));

			v.position = rot(v.position);
			v.normal = {n[0], n[1], v.normal[2]};
		}
	}

	cannon_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
}

void tanks_scene::create_item_meshes()
{
	{
		medicine_mesh = create_lit_mesh();

		geom::vector n{0.f, 0.f, 1.f};
		gfx::color_rgba c{255, 255, 255, 255};
		float w = 0.4f;

		std::vector<lit_vertex> vertices;
		vertices.push_back({{ -w, -w}, n, c});
		vertices.push_back({{  w, -w}, n, c});
		vertices.push_back({{ -w,  w}, n, c});
		vertices.push_back({{ -w,  w}, n, c});
		vertices.push_back({{  w, -w}, n, c});
		vertices.push_back({{  w,  w}, n, c});

		c = {255, 0, 0, 255};
		float s = 0.3f;
		float r = 0.1f;
		vertices.push_back({{ -s, -r}, n, c});
		vertices.push_back({{  s, -r}, n, c});
		vertices.push_back({{ -s,  r}, n, c});
		vertices.push_back({{ -s,  r}, n, c});
		vertices.push_back({{  s, -r}, n, c});
		vertices.push_back({{  s,  r}, n, c});

		vertices.push_back({{ -r, -s}, n, c});
		vertices.push_back({{  r, -s}, n, c});
		vertices.push_back({{ -r,  s}, n, c});
		vertices.push_back({{ -r,  s}, n, c});
		vertices.push_back({{  r, -s}, n, c});
		vertices.push_back({{  r,  s}, n, c});

		medicine_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		upgrade_mesh = create_lit_mesh();

		geom::vector n{0.f, 0.f, 1.f};
		gfx::color_rgba c{0, 127, 0, 255};
		float w = 0.4f;

		std::vector<lit_vertex> vertices;
		vertices.push_back({{ -w, -w}, n, c});
		vertices.push_back({{  w, -w}, n, c});
		vertices.push_back({{ -w,  w}, n, c});
		vertices.push_back({{ -w,  w}, n, c});
		vertices.push_back({{  w, -w}, n, c});
		vertices.push_back({{  w,  w}, n, c});

		c = {255, 255, 0, 255};

		float s = 0.3f;
		float r = 0.1f;
		vertices.push_back({{ -s, -r}, n, c});
		vertices.push_back({{  s, -r}, n, c});
		vertices.push_back({{ -s,  r}, n, c});
		vertices.push_back({{ -s,  r}, n, c});
		vertices.push_back({{  s, -r}, n, c});
		vertices.push_back({{  s,  r}, n, c});

		vertices.push_back({{ -r, -s}, n, c});
		vertices.push_back({{  r, -s}, n, c});
		vertices.push_back({{ -r,  s}, n, c});
		vertices.push_back({{ -r,  s}, n, c});
		vertices.push_back({{  r, -s}, n, c});
		vertices.push_back({{  r,  s}, n, c});

		upgrade_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		autosave_mesh = create_lit_mesh();

		geom::vector n{0.f, 0.f, 1.f};
		gfx::color_rgba c{0, 0, 127, 255};

		std::vector<lit_vertex> vertices;
		vertices.push_back({{ -0.4f, -0.4f}, n, c});
		vertices.push_back({{  0.4f, -0.4f}, n, c});
		vertices.push_back({{  0.4f,  0.2f}, n, c});
		vertices.push_back({{ -0.4f, -0.4f}, n, c});
		vertices.push_back({{  0.4f,  0.2f}, n, c});
		vertices.push_back({{  0.2f,  0.4f}, n, c});
		vertices.push_back({{ -0.4f, -0.4f}, n, c});
		vertices.push_back({{  0.2f,  0.4f}, n, c});
		vertices.push_back({{ -0.4f,  0.4f}, n, c});

		c = {191, 191, 191, 255};
		vertices.push_back({{ -0.2f, 0.3f}, n, c});
		vertices.push_back({{  0.2f, 0.3f}, n, c});
		vertices.push_back({{ -0.2f, 0.4f}, n, c});
		vertices.push_back({{ -0.2f, 0.4f}, n, c});
		vertices.push_back({{  0.2f, 0.3f}, n, c});
		vertices.push_back({{  0.2f, 0.4f}, n, c});

		vertices.push_back({{ -0.2f, -0.3f}, n, c});
		vertices.push_back({{  0.2f, -0.3f}, n, c});
		vertices.push_back({{ -0.2f,  0.0f}, n, c});
		vertices.push_back({{ -0.2f,  0.0f}, n, c});
		vertices.push_back({{  0.2f, -0.3f}, n, c});
		vertices.push_back({{  0.2f,  0.0f}, n, c});

		autosave_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}

	{
		crystal_mesh = create_lit_mesh();

		gfx::color_rgba c{255, 0, 0, 255};

		float s = 0.4f;

		std::vector<lit_vertex> vertices;

		vertices.push_back({{0.f, 0.f}, geom::normalized(geom::vector{1.f, 1.f, 1.f}), c});
		vertices.push_back({{  s, 0.f}, geom::normalized(geom::vector{1.f, 1.f, 1.f}), c});
		vertices.push_back({{0.f,   s}, geom::normalized(geom::vector{1.f, 1.f, 1.f}), c});

		vertices.push_back({{0.f, 0.f}, geom::normalized(geom::vector{-1.f, 1.f, 1.f}), c});
		vertices.push_back({{0.f,   s}, geom::normalized(geom::vector{-1.f, 1.f, 1.f}), c});
		vertices.push_back({{ -s, 0.f}, geom::normalized(geom::vector{-1.f, 1.f, 1.f}), c});

		vertices.push_back({{0.f, 0.f}, geom::normalized(geom::vector{-1.f, -1.f, 1.f}), c});
		vertices.push_back({{ -s, 0.f}, geom::normalized(geom::vector{-1.f, -1.f, 1.f}), c});
		vertices.push_back({{0.f,  -s}, geom::normalized(geom::vector{-1.f, -1.f, 1.f}), c});

		vertices.push_back({{0.f, 0.f}, geom::normalized(geom::vector{ 1.f, -1.f, 1.f}), c});
		vertices.push_back({{0.f,  -s}, geom::normalized(geom::vector{ 1.f, -1.f, 1.f}), c});
		vertices.push_back({{  s, 0.f}, geom::normalized(geom::vector{ 1.f, -1.f, 1.f}), c});

		crystal_mesh.load(vertices, gl::TRIANGLES, gl::STATIC_DRAW);
	}
}

void tanks_scene::switch_room(int dir)
{
	static geom::vector<int, 2> const directions[4] =
	{
		{1, 0},
		{0, 1},
		{-1, 0},
		{0, -1},
	};

	auto const id = current_room_id + directions[dir];

	if (!rooms.contains(id))
	{
		std::uint64_t seed = map_seed;
		util::hash_combine(seed, id[0]);
		util::hash_combine(seed, id[1]);
		random::generator rng{seed, 0};

		bool crystal_room = random::uniform<float>(rng, {0.f, 1.f}) < 0.1f;

		room r;

		if (crystal_room)
		{
			r.tiles.resize({ROOM_SIZE_RANGE.max, ROOM_SIZE_RANGE.max}, tile::ground);
			for (int i = 0; i < 4; ++i)
			{
				auto nid = id + directions[i];
				if (rooms.contains(nid))
					r.exits[i] = rooms[nid].exits[(i + 2) % 4];
				else
					r.exits[i] = true;
			}
			add_border_walls(r.tiles, r.exits);

			r.tiles_hp.resize(r.tiles.dims(), 1.f);

			int s = r.tiles.width() / 2;
			int w = 5;

			for (int x = s - w; x <= s + w; x += 2)
			{
				r.cannons.push_back({{x + 0.5f, s - w + 0.5f}});
				r.cannons.push_back({{x + 0.5f, s + w + 0.5f}});
			}

			for (int y = s - w; y <= s + w; y += 2)
			{
				r.cannons.push_back({{s - w + 0.5f, y + 0.5f}});
				r.cannons.push_back({{s + w + 0.5f, y + 0.5f}});
			}

			for (auto & c : r.cannons)
			{
				c.turret_direction = geom::angle(c.position - geom::point{r.tiles.width() / 2.f, r.tiles.height() / 2.f});
			}

			for (int x = s - w + 1; x < s + w; x += 2)
			{
				r.tiles(x, s - w) = tile::wall;
				r.tiles(x, s + w) = tile::wall;
			}

			for (int y = s - w + 1; y < s + w; y += 2)
			{
				r.tiles(s - w, y) = tile::wall;
				r.tiles(s + w, y) = tile::wall;
			}

			r.crystal = crystal{{s + 0.5f, s + 0.5f}};
		}
		else
		{
			int sizex = random::uniform<int>(rng, {ROOM_SIZE_RANGE.min / 2, ROOM_SIZE_RANGE.max / 2}) * 2 + 1;
			int sizey = random::uniform<int>(rng, {ROOM_SIZE_RANGE.min / 2, ROOM_SIZE_RANGE.max / 2}) * 2 + 1;

			r.tiles.resize({sizex, sizey}, tile::ground);
			r.tiles_hp.resize(r.tiles.dims(), 1.f);

			while (true)
			{
				int free_exits_delta = 0;
				for (int i = 0; i < 4; ++i)
				{
					auto nid = id + directions[i];
					if (rooms.contains(nid))
					{
						r.exits[i] = rooms[nid].exits[(i + 2) % 4];
						free_exits_delta -= (r.exits[i] ? 1 : 0);
					}
					else
					{
						r.exits[i] = random::uniform<float>(rng, {0.f, 1.f}) < 0.5f;
						free_exits_delta += (r.exits[i] ? 1 : 0);
					}
				}

				if (free_exits + free_exits_delta > 0)
				{
					free_exits += free_exits_delta;
					break;
				}
			}
			add_border_walls(r.tiles, r.exits);

			auto inside = [&](geom::point<int, 2> const & p)
			{
				if (p[0] < 1 || p[0] + 1 >= sizex || p[1] < 1 || p[1] + 1 >= sizey)
					return false;
				return true;
			};

			while (true)
			{
				if (random::uniform<float>(rng, {0.f, 1.f}) < 0.1f)
					break;

				int xstart = random::uniform<int>(rng, {1, sizex - 2});
				int ystart = random::uniform<int>(rng, {1, sizey - 2});

				geom::vector<int, 2> dir = directions[random::uniform<int>(rng, {0, 3})];

				int length = random::uniform<int>(rng, {1, std::max(sizex, sizey)});

				auto type = static_cast<tile>(random::uniform<int>(rng, {1, static_cast<int>(tile::count) - 1}));

				for (int i = 0; i < length; ++i)
				{
					geom::point p{xstart + dir[0] * i, ystart + dir[1] * i};

					if (!inside(p))
						break;

					r.tiles(p[0], p[1]) = type;
				}
			}

			std::set<geom::point<int, 2>> convertible_tiles;
			for (int y = 1; y + 1 < sizey; ++y)
			{
				for (int x = 1; x + 1 < sizex; ++x)
				{
					if (!passable(r.tiles(x, y)) && !destructible(r.tiles(x, y)))
						convertible_tiles.insert({x, y});
				}
			}

			auto reachable = [&](geom::point<int, 2> const & p1, geom::point<int, 2> const & p2)
			{
				std::deque<geom::point<int, 2>> q;
				q.push_back(p1);

				std::set<geom::point<int, 2>> visited;

				while (!q.empty())
				{
					auto p = q.front();
					q.pop_front();

					if (visited.contains(p)) continue;
					visited.insert(p);

					for (int d = 0; d < 4; ++d)
					{
						auto n = p + directions[d];
						if (n == p2)
							return true;
						if (inside(n) && !visited.contains(n))
						{
							auto t = r.tiles(n[0], n[1]);
							if (passable(t) || destructible(t))
							{
								q.push_back(n);
							}
						}
					}
				}

				return false;
			};

			geom::point<int, 2> exits_pos[4];
			exits_pos[0] = {sizex - 1, sizey / 2};
			exits_pos[1] = {sizex / 2, sizey - 1};
			exits_pos[2] = {0, sizey / 2};
			exits_pos[3] = {sizex / 2, 0};

			auto exits_reachable = [&]{
				for (int i = 0; i < 4; ++i)
				{
					if (!r.exits[i]) continue;
					for (int j = i + 1; j < 4; ++j)
					{
						if (!r.exits[j]) continue;
						if (!reachable(exits_pos[i], exits_pos[j]))
							return false;
					}
				}

				return true;
			};

			while (!exits_reachable() && !convertible_tiles.empty())
			{
				auto i = random::uniform<int>(rng, {0, convertible_tiles.size() - 1});
				auto it = std::next(convertible_tiles.begin(), i);

				auto p = *it;
				convertible_tiles.erase(it);

				r.tiles(p[0], p[1]) = tile::barricade;
			}

			std::set<geom::point<int, 2>> free_tiles;
			for (int y = 1; y + 1 < sizey; ++y)
			{
				for (int x = 1; x + 1 < sizex; ++x)
				{
					if (r.tiles(x, y) == tile::ground)
						free_tiles.insert({x, y});
				}
			}

			while (true)
			{
				if (free_tiles.empty())
					break;

				if (random::uniform<float>(rng, {0.f, 1.f}) < 0.5f)
					break;

				int i = random::uniform<int>(rng, {0, free_tiles.size() - 1});

				auto it = std::next(free_tiles.begin(), i);
				free_tiles.erase(it);

				r.cannons.push_back({{(*it)[0] + 0.5f, (*it)[1] + 0.5f}});
			}

			while (true)
			{
				if (free_tiles.empty())
					break;

				if (random::uniform<float>(rng, {0.f, 1.f}) < 0.5f)
					break;

				int i = random::uniform<int>(rng, {0, free_tiles.size() - 1});

				auto it = std::next(free_tiles.begin(), i);
				free_tiles.erase(it);

				r.medicines.push_back({{(*it)[0] + 0.5f, (*it)[1] + 0.5f}});
			}

			while (true)
			{
				if (free_tiles.empty())
					break;

				if (random::uniform<float>(rng, {0.f, 1.f}) < 0.5f)
					break;

				int i = random::uniform<int>(rng, {0, free_tiles.size() - 1});

				auto it = std::next(free_tiles.begin(), i);
				free_tiles.erase(it);

				auto t = static_cast<upgrade::type_t>(random::uniform<int>(rng, {0, static_cast<int>(upgrade::type_t::count) - 1}));

				r.upgrades.push_back({{(*it)[0] + 0.5f, (*it)[1] + 0.5f}, t});
			}

			while (true)
			{
				if (free_tiles.empty())
					break;

				if (random::uniform<float>(rng, {0.f, 1.f}) < 0.5f)
					break;

				int i = random::uniform<int>(rng, {0, free_tiles.size() - 1});

				auto it = std::next(free_tiles.begin(), i);
				free_tiles.erase(it);

				r.autosaves.push_back({{(*it)[0] + 0.5f, (*it)[1] + 0.5f}});
			}
		}

		rooms[id] = std::move(r);
	}

	auto const & r1 = rooms.at(current_room_id);
	auto const & r2 = rooms.at(id);

	auto exit1 = r1.bbox().center() + geom::pointwise_mult(geom::cast<float>( directions[dir]), r1.bbox().dimensions() / 2.f);
	auto exit2 = r2.bbox().center() + geom::pointwise_mult(geom::cast<float>(-directions[dir]), r2.bbox().dimensions() / 2.f);

	player_tank.position = (player_tank.position - exit1) + exit2 + geom::cast<float>(directions[dir]);

	current_room_id = id;
	on_resize(width(), height());
	update_minimap();

	explosions.clear();
	bullets.clear();

	for (auto & c : rooms[current_room_id].cannons)
		c.bullet_reload = c.max_bullet_reload;
}

void tanks_scene::update_minimap()
{
	geom::box<int, 2> id_range;
	for (auto const & p : rooms)
		id_range |= p.first;

	int const cell = 11;
	int const w = 2;
	int const e = 3;
	int const s = 3;

	gfx::pixmap_rgba pm({cell * (id_range[0].length() + 1), cell * (id_range[1].length() + 1)}, {0, 0, 0, 0});

	for (auto const & p : rooms)
	{
		auto id = p.first;

		auto at = [&](int x, int y) -> gfx::color_rgba & {
			return pm((id[0] - id_range[0].min) * cell + x, (id[1] - id_range[1].min) * cell + y);
		};

		for (int y = w; y + w < cell; ++y)
		{
			for (int x = w; x + w < cell; ++x)
			{
				at(x, y) = {255, 255, 255, 255};
			}
		}

		std::optional<gfx::color_rgba> c;
		if (id == autosave.current_room)
			c = {0, 0, 127, 255};
		if (p.second.crystal)
			c = {255, 0, 0, 255};
		if (id == current_room_id)
			c = {0, 191, 0, 255};

		if (c)
		{
			for (int y = s; y + s < cell; ++y)
			{
				for (int x = s; x + s < cell; ++x)
				{
					at(x, y) = *c;
				}
			}
		}

		if (p.second.exits[0])
		{
			for (int y = e; y + e < cell; ++y)
			{
				for (int x = cell - w; x < cell; ++x)
				{
					at(x, y) = {255, 255, 255, 255};
				}
			}
		}

		if (p.second.exits[1])
		{
			for (int x = e; x + e < cell; ++x)
			{
				for (int y = cell - w; y < cell; ++y)
				{
					at(x, y) = {255, 255, 255, 255};
				}
			}
		}

		if (p.second.exits[2])
		{
			for (int y = e; y + e < cell; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					at(x, y) = {255, 255, 255, 255};
				}
			}
		}

		if (p.second.exits[3])
		{
			for (int x = e; x + e < cell; ++x)
			{
				for (int y = 0; y < w; ++y)
				{
					at(x, y) = {255, 255, 255, 255};
				}
			}
		}
	}

	minimap_texture.load(pm);
}

void tanks_scene::save()
{
	autosave.rooms = rooms;
	autosave.free_exits = free_exits;
	autosave.current_room = current_room_id;
	autosave.player = player_tank;
	autosave.player.bullet_reload = player_tank.reload_time;
	for (auto & c: autosave.rooms[autosave.current_room].cannons)
		c.bullet_reload = c.max_bullet_reload;
}

void tanks_scene::load()
{
	rooms = autosave.rooms;
	free_exits = autosave.free_exits;
	current_room_id = autosave.current_room;
	player_tank = autosave.player;
	bullets.clear();
	explosions.clear();
	on_resize(width(), height());
	update_minimap();
}

void tanks_scene::new_game()
{
	random::device dev;
	map_seed = dev() | (static_cast<std::uint64_t>(dev()) << 32);
	log::info() << "Map seed: " << std::hex << std::setfill('0') << std::setw(16) << map_seed;

	rooms.clear();
	bullets.clear();
	explosions.clear();

	rooms[STARTING_ROOM_ID] = make_starting_room();
	free_exits = 1;
	current_room_id = STARTING_ROOM_ID;

	auto const & current_room = rooms[current_room_id];

	player_tank = tank{};
	player_tank.position = {current_room.tiles.width() / 2.f, current_room.tiles.height() / 2.f};
	player_tank.color = {0.f, 1.f, 0.f};

	update_minimap();
	save();

	on_resize(width(), height());
}

void tanks_scene::show_death_dialog()
{
	auto d = element_factory.make_frame();
	auto l = element_factory.make_grid_layout();
	d->set_child(l);

	auto load_button = element_factory.make_button("Load last save");
	auto new_game_button = element_factory.make_button("New game");
	auto quit_button = element_factory.make_button("Quit");

	load_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); load(); });
	new_game_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); new_game(); });
	quit_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); on_quit(); });

	auto label = element_factory.make_label("You are dead!");
	label->set_halign(ui::label::halignment::center);

	l->set_row_count(4);
	l->set(0, 0, label);
	l->set(1, 0, load_button);
	l->set(2, 0, new_game_button);
	l->set(3, 0, quit_button);
	ui_root->add_child(d, ui::screen::x_policy::center, ui::screen::y_policy::center);
}

void tanks_scene::show_victory_dialog()
{
	auto d = element_factory.make_frame();
	auto l = element_factory.make_grid_layout();
	d->set_child(l);

	auto new_game_button = element_factory.make_button("New game");
	auto quit_button = element_factory.make_button("Quit");

	new_game_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); new_game(); });
	quit_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); on_quit(); });

	auto label = element_factory.make_label("You won!");
	label->set_halign(ui::label::halignment::center);

	l->set_row_count(3);
	l->set(0, 0, label);
	l->set(1, 0, new_game_button);
	l->set(2, 0, quit_button);
	ui_root->add_child(d, ui::screen::x_policy::center, ui::screen::y_policy::center);
}

void tanks_scene::show_esc_dialog()
{
	auto d = element_factory.make_frame();
	auto l = element_factory.make_grid_layout();
	d->set_child(l);

	auto continue_button = element_factory.make_button("Continue");
	auto new_game_button = element_factory.make_button("New game");
	auto quit_button = element_factory.make_button("Quit");

	continue_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); });
	new_game_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); new_game(); });
	quit_button->on_click([this]{ audio_engine.play(click_track); remove_dialog(); on_quit(); });

	auto label = element_factory.make_label("Paused");
	label->set_halign(ui::label::halignment::center);

	l->set_row_count(4);
	l->set(0, 0, label);
	l->set(1, 0, continue_button);
	l->set(2, 0, new_game_button);
	l->set(3, 0, quit_button);
	ui_root->add_child(d, ui::screen::x_policy::center, ui::screen::y_policy::center);
}

void tanks_scene::remove_dialog()
{
	std::vector<ui::element *> cs;
	for (auto c : ui_root->children())
		if (c) cs.push_back(c);
	for (auto c : cs)
		ui_root->remove_child(c);
}

bool tanks_scene::has_dialog()
{
	for (auto c : ui_root->children())
		if (c)
			return true;
	return false;
}

struct tanks_app
	: app::app
{
	ui::controller controller;
	async::event_loop loop;

	tanks_app()
		: app("Tanks!", 4)
		, controller(&loop)
	{
		push_scene(std::make_unique<tanks_scene>(controller, [this]{ stop(); }));
	}
};

int main()
{
	return app::main<tanks_app>();
}
